package no.conta.docker;

import com.bmuschko.gradle.docker.tasks.RegistryCredentialsAware;
import com.bmuschko.gradle.docker.tasks.image.DockerBuildImage;
import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.util.List;

public class DockerConventionsPlugin implements Plugin<Project> {

    public static final String ENV_CI_REGISTRY = "CI_REGISTRY";
    public static final String ENV_CI_PROJECT_PATH = "CI_PROJECT_PATH";
    public static final String ENV_CI_REGISTRY_USER = "CI_REGISTRY_USER";
    public static final String ENV_CI_REGISTRY_PASSWORD = "CI_REGISTRY_PASSWORD";

    public static final String PROPERTY_DOCKER_PLATFORM = "dockerPlatform";
    public static final String INPUT_PROPERTY_RESOLVED_DOCKER_PLATFORM = "resolvedDockerPlatform";

    @Override
    public void apply(Project project) {

        String ciDockerRegistry = System.getenv(ENV_CI_REGISTRY);

        List<String> armArch = List.of("aarch64", "arm64");
        String dockerPlatform = (String) project.findProperty(PROPERTY_DOCKER_PLATFORM);
        if (dockerPlatform == null) {
            dockerPlatform = System.getProperty("os.arch");
        }

        String resolvedDockerPlatform = armArch.contains(dockerPlatform) ? "linux/arm64" : "linux/amd64";

        project.getTasks().withType(DockerBuildImage.class, t -> {

        });
        project.getTasks().withType(DockerBuildImage.class).configureEach(
            new DockerBuildImageAction(project, ciDockerRegistry, resolvedDockerPlatform)
        );

        if (runningOnCi(ciDockerRegistry)) {
            project.getTasks().withType(RegistryCredentialsAware.class).configureEach(rca ->
                rca.registryCredentials(drc -> {
                    drc.getUrl().set(ciDockerRegistry);
                    drc.getUsername().set(System.getenv(ENV_CI_REGISTRY_USER));
                    drc.getPassword().set(System.getenv(ENV_CI_REGISTRY_PASSWORD));
                })
            );
        }
    }

    private static boolean runningOnCi(String ciDockerRegistry) {
        return ciDockerRegistry != null && !ciDockerRegistry.isBlank();
    }

    private static class DockerBuildImageAction implements Action<DockerBuildImage> {

        private final Project project;
        private final String registry;
        private final String platform;

        public DockerBuildImageAction(Project project, String ciDockerRegistry, String resolvedDockerPlatform) {
            this.project = project;
            this.registry = ciDockerRegistry;
            this.platform = resolvedDockerPlatform;
        }

        @Override
        public void execute(DockerBuildImage t) {
            t.getInputs().property(INPUT_PROPERTY_RESOLVED_DOCKER_PLATFORM, platform);
            t.getPlatform().set(platform);

            t.getPull().set(true); // we always pull to get updated image when switching platforms between arm64 and amd64

            if (runningOnCi(registry)) {
                String ciProjectPath = System.getenv(ENV_CI_PROJECT_PATH);
                String image = String.format("%s/%s/%s:%s", registry, ciProjectPath, project.getName(), project.getVersion());
                t.getImages().set(List.of(image));
                t.doFirst(a -> project.getLogger().lifecycle("Pushing image [{}]", image));
            } else {
                t.getImages().set(List.of(
                    String.format("%s:%s", project.getName(), project.getVersion()),
                    String.format("localhost:5000/%s:%s", project.getName(), project.getVersion()),
                    String.format("%s:latest", project.getName()) // we're only tagging 'latest' locally
                ));
            }
        }

    }
}
