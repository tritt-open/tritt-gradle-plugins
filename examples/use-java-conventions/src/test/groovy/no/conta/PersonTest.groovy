package no.conta

import spock.lang.Specification

import java.time.LocalDate

class PersonTest extends Specification {

    def 'create person using creator method'() {
        when:
            Person p = Person.of('Ada', LocalDate.parse('1815-12-10'))
        then:
            p.name == 'Ada'
            p.bornAt != null
    }
}
