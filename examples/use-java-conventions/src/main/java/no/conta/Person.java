package no.conta;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor(staticName = "of")
public class Person {

    private final String name;
    private final LocalDate bornAt;

}
