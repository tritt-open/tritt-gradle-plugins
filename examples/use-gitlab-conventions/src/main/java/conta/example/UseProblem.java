package conta.example;

import no.conta.problem.Problem;

public class UseProblem {

    private final Problem problem;

    public UseProblem(Problem problem) {
        this.problem = problem;
    }

    public Problem getProblem() {
        return problem;
    }
}
