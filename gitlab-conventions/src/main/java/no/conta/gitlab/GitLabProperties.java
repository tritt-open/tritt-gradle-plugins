package no.conta.gitlab;

import org.gradle.api.Project;

import java.util.Optional;

public class GitLabProperties implements GitLabAware {

    private final Project project;

    public GitLabProperties(Project project) {
        this.project = project;
    }

    @Override
    public Optional<String> getGroupId() {
        return property(PROPERTY_GITLAB_GROUP_ID);
    }

    @Override
    public Optional<String> getProjectId() {
        return property(PROPERTY_GITLAB_PROJECT_ID);
    }

    @Override
    public Optional<String> getToken() {
        return property(PROPERTY_GITLAB_TOKEN)
            .or(() -> Optional.ofNullable(System.getenv(ENV_VAR_GITLAB_TOKEN)));
    }

    public Optional<String> getPublishToProjectId() {

        boolean publish = property(PROPERTY_GITLAB_PUBLISH_TO_PROJECT)
            .map(Boolean::parseBoolean)
            .orElse(true);

        return publish ?
            property(PROPERTY_GITLAB_PUBLISH_TO_PROJECT_ID).or(this::getProjectId) :
            Optional.empty();
    }

    private Optional<String> property(String id) {
        Object property = project.findProperty(id);
        return property == null ? Optional.empty() : Optional.of(property.toString());
    }
}
