package no.conta.gitlab;

public enum GitLabGroup {
    Conta("6306164"),
    ContaOpen("12992043");

    public final String groupId;

    GitLabGroup(String groupId) {
        this.groupId = groupId;
    }
}
