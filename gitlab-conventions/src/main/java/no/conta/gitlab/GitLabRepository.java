package no.conta.gitlab;

import javax.inject.Inject;
import java.util.Optional;

public class GitLabRepository implements GitLabAware {

    public final String name;
    public String groupId;
    public String projectId;
    public String token;

    @Inject
    public GitLabRepository(String name) {
        this.name = name;
    }

    @Override
    public Optional<String> getGroupId() {
        return Optional.ofNullable(groupId);
    }

    @Override
    public Optional<String> getProjectId() {
        return Optional.ofNullable(projectId);
    }

    @Override
    public Optional<String> getToken() {
        return Optional.ofNullable(token);
    }

    public void groupId(String groupId) {
        this.groupId = groupId;
    }

    public void projectId(String projectId) {
        this.projectId = projectId;
    }

    public void token(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "GitLabRepository{" +
            "name='" + name + '\'' +
            "groupId='" + getGroupId().orElse("") + '\'' +
            '}';
    }
}
