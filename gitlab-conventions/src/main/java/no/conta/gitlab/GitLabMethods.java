package no.conta.gitlab;

import org.gradle.api.GradleException;
import org.gradle.api.Project;

import static no.conta.gitlab.GitLabAware.ENV_VAR_GITLAB_TOKEN;
import static no.conta.gitlab.GitLabAware.PROPERTY_GITLAB_TOKEN;

public class GitLabMethods {

    private GitLabMethods() {
    }

    static String gitLabToken(Project project) {
        Object property = project.findProperty(PROPERTY_GITLAB_TOKEN);
        if (property != null) {
            return property.toString();
        }
        String var = System.getenv(ENV_VAR_GITLAB_TOKEN);
        return var == null ? "" : var;
    }

    static String createRepositoryUrl(String groupId, String projectId) {
        if (groupId != null && !groupId.isBlank()) {
            return createGroupRepositoryUrl(groupId);
        }
        if (projectId != null && !projectId.isBlank()) {
            return createProjectRepositoryUrl(projectId);
        }
        throw new GradleException("Cannot add GitLab repository without groupId or projectId");
    }

    static String createGroupRepositoryUrl(String groupId) {
        return String.format("https://gitlab.com/api/v4/groups/%s/-/packages/maven", groupId);
    }

    static String createProjectRepositoryUrl(String projectId) {
        return String.format("https://gitlab.com/api/v4/projects/%s/packages/maven", projectId);
    }
}
