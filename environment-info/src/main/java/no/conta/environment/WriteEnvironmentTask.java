package no.conta.environment;

import org.gradle.api.DefaultTask;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.*;


public abstract class WriteEnvironmentTask extends DefaultTask {

    @Input
    public abstract Property<String> getVersion();

    @OutputFile
    public abstract RegularFileProperty getOutputFile();

    @TaskAction
    public void readEnvironment() throws IOException {
        try(PrintWriter writer = new PrintWriter(new FileWriter(getOutputFile().getAsFile().get()))) {
            writer.println("PROJECT_VERSION=" + getVersion().get());
        }
    }
}
