package no.conta.environment;

import org.gradle.api.DefaultTask;
import org.gradle.api.file.RegularFileProperty;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.TaskAction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public abstract class PrintEnvironmentTask extends DefaultTask {

    @InputFile
    public abstract RegularFileProperty getInputFile();

    @TaskAction
    public void printEnvironment() throws IOException {
        try(BufferedReader reader = new BufferedReader(new FileReader(getInputFile().getAsFile().get()))) {
            reader.lines().forEach(System.out::println);
        }
    }

}
