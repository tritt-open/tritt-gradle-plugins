package no.conta.micronaut;

import io.micronaut.gradle.MicronautMinimalLibraryPlugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.PluginManager;

public class MicronautLibraryConventionsPlugin extends MicronautConventionsPlugin {

    @Override
    public void apply(Project project) {

        PluginManager pluginManager = project.getPluginManager();
        pluginManager.apply(MicronautMinimalLibraryPlugin.class);

        applyCommonConventions(project);
    }
}
