package no.conta.micronaut;

import io.micronaut.gradle.MicronautMinimalApplicationPlugin;
import io.micronaut.gradle.docker.MicronautDockerPlugin;
import io.micronaut.gradle.docker.MicronautDockerfile;
import org.gradle.api.Project;
import org.gradle.api.plugins.PluginManager;

public class MicronautApplicationConventionsPlugin extends MicronautConventionsPlugin {

    @Override
    public void apply(Project project) {

        PluginManager pluginManager = project.getPluginManager();
        pluginManager.apply(MicronautMinimalApplicationPlugin.class);
        pluginManager.apply(MicronautDockerPlugin.class);

        project.getTasks().withType(MicronautDockerfile.class).configureEach(mdf -> {
                Object dockerBaseImage = project.findProperty("dockerBaseImage");
                if (dockerBaseImage != null) {
                    mdf.baseImage(dockerBaseImage.toString());
                }
                project.getTasksByName("assemble", false).forEach(assembleTask ->
                    assembleTask.dependsOn(mdf)
                );
            }
        );

        applyCommonConventions(project);
    }
}
