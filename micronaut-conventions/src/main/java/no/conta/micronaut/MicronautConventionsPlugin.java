package no.conta.micronaut;

import io.micronaut.gradle.MicronautExtension;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.util.List;

public abstract class MicronautConventionsPlugin implements Plugin<Project> {

    protected void applyCommonConventions(Project project) {
        MicronautExtension extension = project.getExtensions().getByType(MicronautExtension.class);

        extension.version((String) project.property("micronautVersion"));
        extension.processing(p -> {
            p.getIncremental().convention(true);
            p.getAnnotations().convention(List.of("conta.*"));
        });
    }

}
